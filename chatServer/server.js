var io = require('socket.io'),
  connect = require('connect'),
  user = require('user');

/* Configure Options for Socket.IO */


var app = connect().use(connect.static('public')).listen(3000);
var chat_room = io.listen(app);

user.set_sockets(chat_room.sockets);

chat_room.sockets.on('connection', function (socket) {
  user.connect_user({
    socket: socket,
    username: socket.id
  });
});

/*
var io = require('socket.io'),
  connect = require('connect');

var app = connect().use(connect.static('public')).listen(3000);
var chat_room = io.listen(app);

chat_room.sockets.on('connection', function (socket) {
  socket.emit('entrance', {message: 'Velkommen til IT-Guidens chat!'});

  socket.on('disconnect', function  () {
    chat_room.sockets.emit('exit', {message: ' has disconnected.'});
  });

  socket.on('chat', function  (data) {
    chat_room.sockets.emit('chat', {message: '# ' + data.message});
  });

  chat_room.sockets.emit('entrance', {message: 'A new chatter is online.'});
});*/