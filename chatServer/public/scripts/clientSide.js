$(document).ready(function () {
 
	
	
	var log_chat_message = function  (message, type) {
		
		// TODO: Fix short date format
		var today = new Date();
		var dateTimeNow = today.toUTCString();
		var messageType = "";
		var li = $('<li />');
		
		// socket on entrance
		if (type === 'system') {
			li.css({'font-weight': 'bold'});
					messageType = "SERVER: ";
		// socket on exit
		} else if (type === 'leave') {
			li.css({'font-weight': 'bold', 'color': '#F00'});
					messageType = "Noooooooo: ";
		// socket on user	
		} else if (type === 'user') {
			li.css({'font-weight': 'normal', 'color': '#000'});
					
		} 
		li.text(messageType + dateTimeNow + " : " + message);
				
		$('#chat_log').append(li);
	};

	var socket = io.connect('http://5.103.129.240:3000');

	////////// Eventhandlers begin \\\\\\\\\\
	socket.on('entrance', function  (data) {
		log_chat_message(data.message, 'system');
	});

	socket.on('exit', function  (data) {
		log_chat_message(data.message, 'leave');
	});

	socket.on('chat', function  (data) {
		log_chat_message(data.message, 'user');
	});

	// Adds a keyboard listener on the ENTER key (Nr. 13)
	$('#chat_box').keypress(function (event) {
		if (event.which == 13) {
			sendNewLine();
		}
	});
	
	// Adds a onClick listener to the sendButton
	$("#sendButton").click(function() {
		sendNewLine();
	});	
	
		// Adds a onClick listener to the sendButton
	$("#frontBtn").click(function() {
		selectCard();
	});	
	
	$("#backBtn").click(function() {
		selectCardReverse();
	});
	
	$("#smiley").click(function() {
		insertIcon("smiley");
	});
	$("#sad").click(function() {
		insertIcon("sad");
	});
	
	// Send(emit) the text entered in the chat_box to the socket
	function sendNewLine() {
		socket.emit('chat', {message: $('#chat_box').val()});
		$('#chat_box').val('');	
		scrollToLastInput();
	};
		
});

function insertIcon(iconImage) {
	
	/*$('#chat_box').val($($('#chat_box').val()).css("background", "url(../images/happy_smiley_face.png)"));*/

	var icon = iconImage ;
	switch (icon)
	{
	case "smiley":
	  $("#chat_log > li:last-child").append($("<img class='iconImage' src='../images/happy_smiley_face.png'/>"));
	  break;
	case "sad":
	  $("#chat_log > li:last-child").append($("<img class='iconImage' src='../images/sad_smiley_face.png'/>"));
	  break;
	}


}

function scrollToLastInput() {
	var div = $("#chatRoomSection")[0];
	var scrollHeight = Math.max(div.scrollHeight, div.clientHeight);
	div.scrollTop = scrollHeight - div.clientHeight;

}




function selectCard() {

	$(".card").addClass("card-flipped");
	// check the pattern of both flipped card 0.7s later.

	}
	function selectCardReverse() {

	$(".card").removeClass("card-flipped");
	// check the pattern of both flipped card 0.7s later.

	}